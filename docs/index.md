# Disaster Documentation

## Setup

### Plaiframe and Database

Plaiframe connects devices, sound and light and serves an interface to control the shows.

The software requires you to install [nodejs](https://nodejs.org/en) and [mongodb](https://www.mongodb.com/)

There are two ways to download the disaster plaiframe. Using git clone requires you to have git installed.  Using the .zip file download will make it harder to update the project.

#### Get source files using git clone

Clone the Plaiframe repository from https://gitlab.com/machina_ex/legacy/plaiframe

the Disaster files will not work with the latest version so you need to checkout the repository at the `disaster-nz` branch:

```
git clone --branch disaster-nz git@gitlab.com:machina_ex/legacy/plaiframe.git
```

#### Get .zip source file via download

Download the files from here: [plaiframe-disaster-nz.zip](https://gitlab.com/machina_ex/legacy/plaiframe/-/archive/disaster-nz/plaiframe-disaster-nz.zip)

unzip the files and `cd` into the `plaiframe-disaster-nz` directory

#### Install

Once cloned install dependencies with

```
npm install
```

To install and configure **mongodb** go through the steps in the [MongoDB](https://gitlab.com/machina_ex/legacy/plaiframe/-/tree/disaster?ref_type=tags#mongodb) Chapter in the Plaiframe Repository:

Start by following the instructions on how to [install mongodb community edition](https://docs.mongodb.com/manual/administration/install-community/).

Plaiframe comes with a shell file `mongo.sh` to setup a database with a so called 'replica set' that is needed for use with plaiframe.

> If you run plaiframe on MacOS, use `mongo_macos.sh` instead.

Use it once for setup like this:

`$ sh mongo.sh setup`

and follow the instructions.

To create a default mongodb setup that works well on your local machine press enter for the default value on all input prompts.

this creates required database folders, config files and initiates mongo replica sets which are obligatory for change listeners.

once it is set up, you can always do

`$ sh mongo.sh start`

to start mongodb before you run the node script and

`$ sh mongo.sh stop`

to stop the mongodb process.

#### Setup

Download the [Disaster game files](./assets/disaster-db-files.zip).

Unzip and in the terminal cd into the directory you downloaded the game files into. Use the [mongo restore](https://www.mongodb.com/docs/database-tools/mongorestore/) command line tool to load the files into your database.

```
mongorestore dump/
```

Start the Plaiframe server

```
npm start
```

In the plaiframe command line `>` Load the Disaster Game from the connected database

```
load Disaster
```

Open the web editor in your browser: [http://localhost:8080](http://localhost:8080)

### Sound with Ableton Live

Download the Ableton liveset that contains and arranges the soundfiles for the show here:

[DISASTER ableton 9 Liveset Project.zip](https://cloud.machinaex.org/index.php/s/mBYi2skmKZmSsRD)

To Run it you need [Ableton Live](https://www.ableton.com/en/live/) version 9 or higher.

### Devices

Find the original firmware code for each device on [gitlab.com/machina_ex/disaster](https://gitlab.com/machina_ex/disaster). Each repository contains a brief setup instructions readme.

Some devices receive messages from the plaiframe. Whenever 

#### Zeitbombe

7 Segment LED Timer with Wemos D1 mini controller.

[Repository](https://gitlab.com/machina_ex/disaster/zeitbombe)

Requires Powerbank USB Battery.

If it needed to be changed, adjust the zeitbombe device IP in the plaiframe editor:
[http://localhost:8080/Disaster/editor#zeitbombe](http://localhost:8080/Disaster/editor#zeitbombe)

![Change Device IP](./assets/change_device_ip.png)

#### Pinwriter

Raspberry Pi that controls a dot matrix pinwriter.

[Repository](https://gitlab.com/machina_ex/disaster/pinwriter)

If it needed to be changed, adjust the pinwriter device IP in the plaiframe editor:
[http://localhost:8080/Disaster/editor#pinwriter](http://localhost:8080/Disaster/editor#pinwriter)

#### Licht Task

1. Colored Cables that feedback their connection status.

2. Fuse Switch that can be triggered remotely.

Both connected via Arduino w/ Ethernet shield.

[Repository](https://gitlab.com/machina_ex/disaster/licht-task)

If it needed to be changed, adjust the lichttask device IP in the plaiframe editor:
[http://localhost:8080/Disaster/editor#lichttask](http://localhost:8080/Disaster/editor#lichttask)

#### Telefone

12 Old Telephones containing USB Soundcards.

Connected via USB to a computer running the Ableton Liveset *Konferenz Liveset*

[Repository](https://gitlab.com/machina_ex/disaster/telefone)

[Telefon Konferenz.zip](https://cloud.machinaex.org/index.php/s/mBYi2skmKZmSsRD)

Replace the [liveset_cory device IP](http://localhost:8080/Disaster/editor#liveset_cory) and the [telefone device IP](http://localhost:8080/Disaster/editor#telefone) with the IP of the **telefone** computer in the plaiframe editor.

#### Menschenkette

Touch sensor connected vie ESP32 Controller Board.

To trigger the touch event 3 1.5V Batteries need to be connected in a circuit including human bodie(s)

[Repository](https://gitlab.com/machina_ex/disaster/menschenkette)

If it needed to be changed, adjust the menschenkette device IP in the plaiframe editor:
[http://localhost:8080/Disaster/editor#menschenkette](http://localhost:8080/Disaster/editor#menschenkette)

#### Reset Button

Big Black Push Button with WemosD1 Mini Controller inside.

[Repository](https://gitlab.com/machina_ex/disaster/reset-button)

Requires 3 AAA 1.5V Batteries.

Goes into Sleep Mode to save batteries. Boots back up on Button Push.

If it needed to be changed, adjust the reset_button device IP in the plaiframe editor:
[http://localhost:8080/Disaster/editor#reset_button](http://localhost:8080/Disaster/editor#reset_button)