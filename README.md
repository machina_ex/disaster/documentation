# Disaster Documentation

## Setup

### Plaiframe and Database

Plaiframe connects devices, sound and light and serves an interface to control the shows.

The software requires you to install [nodejs](https://nodejs.org/en) and [mongodb](https://www.mongodb.com/)

Clone the Plaiframe repository from https://gitlab.com/machina_ex/legacy/plaiframe

the Disaster files will not work with the latest version so you need to checkout the repository at the `disaster` tag:

```
git clone --branch disaster git@gitlab.com:machina_ex/legacy/plaiframe.git
```

Once cloned install dependencies with

```
npm install
```

To install mongodb go through the steps in the [MongoDB](https://gitlab.com/machina_ex/legacy/plaiframe/-/tree/disaster?ref_type=tags#mongodb) Chapter in the Plaiframe Repository.

Download the [Disaster game files](./docs/assets/Disaster.agz).

Use the mongo restore command line tool to load the files into your database

```
mongorestore --db Disaster ./Disaster.agz
```

Start the Plaiframe server

```
npm start
```

In the plaiframe command line `>` Load the Disaster Game from the connected database

```
load Disaster
```

Open the web editor in your browser: [http://localhost:8080](http://localhost:8080)

### Sound with Ableton Live

Download the Ableton liveset that contains and arranges the soundfiles for the show here:

[DISASTER ableton 9 Liveset Project.zip](https://cloud.machinaex.org/index.php/s/mBYi2skmKZmSsRD)

To Run it you need [Ableton Live](https://www.ableton.com/en/live/) version 9 or higher.

### Devices

Find the original firmware code for each device on [gitlab.com/machina_ex/disaster](https://gitlab.com/machina_ex/disaster). Each repository contains a brief setup instructions readme.

#### Zeitbombe

7 Segment LED Timer with Wemos D1 mini controller.

[Repository](https://gitlab.com/machina_ex/disaster/zeitbombe)

Requires Powerbank USB Battery.

#### Pinwriter

Raspberry Pi that controls a dot matrix pinwriter.

[Repository](https://gitlab.com/machina_ex/disaster/pinwriter)

#### Licht Task

1. Colored Cables that feedback their connection status.

2. Fuse Switch that can be triggered remotely.

Both connected via Arduino w/ Ethernet shield.

[Repository](https://gitlab.com/machina_ex/disaster/licht-task)

#### Telefone

12 Old Telephones containing USB Soundcards.

Connected via USB to a computer running the Ableton Liveset *Konferenz Liveset*

[Repository](https://gitlab.com/machina_ex/disaster/telefone)

[Telefon Konferenz.zip](https://cloud.machinaex.org/index.php/s/mBYi2skmKZmSsRD)

#### Menschenkette

Touch sensor connected vie ESP32 Controller Board.

To trigger the touch event 3 1.5V Batteries need to be connected in a circuit including human bodie(s)

[Repository](https://gitlab.com/machina_ex/disaster/menschenkette)

#### Reset Button

Big Black Push Button with WemosD1 Mini Controller inside.

[Repository](https://gitlab.com/machina_ex/disaster/reset-button)

Requires 3 AAA 1.5V Batteries.

Goes into Sleep Mode to save batteries. Boots back up on Button Push.